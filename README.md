# Car Management API

Challenge Chapter 6 - Fullstack Web Binar Academy

- Restful API menggunakan Express.js & Mysql
- Authorization menggunakan JWT Token.
- Dokumentasi Open API menggunakan Swagger.

# Installation

1. Install dependencies dengan perintah `npm install --save`
```sh
npm install --save
```
2. Buat database baru dengan nama `db_challenge6`
3. Setup database pada file [`config/config.json`](./config/config.json)
4. Migrate tabel dengan perintah `sequelize db:migrate`
```sh
sequelize db:migrate
```
5. Seed data dengan perintah `sequelize db:seed:all`
```sh
sequelize db:seed:all
```
5. Jalankan server menggunakan nodemon
```sh
npm run dev
```

## Dokumentasi Open API
- Untuk membuka dokumentasi Open API dengan Swagger UI lakukan pada url berikut.
```
http://localhost:8000/api-docs
```
- Untuk membuka dokumentasi Open API dalam format json lakukan pada url berikut.
```
http://localhost:8000/api/json
```

# Super Admin Account
Email:
```
superadmin@gmail.com
```
Password:
```
admin123
```

# ERD
![Car Management API - ERD](./_documents/erd.png)

