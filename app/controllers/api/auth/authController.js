const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const userService = require('../../../services/userService')

function createToken(payload) {
  return jwt.sign(payload, process.env.JWT_SIGNATURE_KEY || 'secret', {
    expiresIn: 10 * 60,
  })
}

module.exports = {
  async login(req, res) {
    const email = req.body.email.toLowerCase()
    const hashedPassword = await bcrypt.hash(req.body.password, 10)

    const user = await userService.get({ where: { email } })
    if (!user) {
      res.status(404).json({ 
        status: false, 
        message: 'Email tidak ditemukan' 
      })
      return
    }

    const check = bcrypt.compare(hashedPassword, user.password, function (
      err,
      result,
    ) {
      if (err) {
        throw err
      }
      return result
    })

    if (check === false) {
      res.status(401).json({
        status: false,
        message: 'Password salah!',
      })
      return
    }

    const token = createToken({
      id: user.id,
      email: user.email,
      role: user.roleId
    })

    res.status(201).json({
      name: user.name,
      email: user.email,
      role: user.roleId,
      token
    })
  },

  async whoAmI(req, res) {
    let role = ''
    if (req.user.roleId === 1) {
      role = 'Superadmin'
    } else if (req.user.roleId === 2) {
      role = 'Admin'
    } else if (req.user.roleId === 3) {
      role = 'Member'
    }
    res.status(201).json({
      status: true,
      message: `Anda adalah ${role}`,
      data: { 
        id: req.user.id, 
        name: req.user.name, 
        email: req.user.email 
      },
    })
  },
}
