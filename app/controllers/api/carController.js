const carService = require('../../services/carService');
const userService = require("../../services/userService");
const logService = require("../../services/logService");
const { Log } = require("../../models");


module.exports = {
  async list(req, res) {
    try {
      const data = await carService.list({
        include: [{ model: Log }],
      });
      res.status(200).json({
        status: true,
        message: 'Show all data car successfully',
        data: data,
      });
    } catch (err) {
      res.status(400).json({
        status: false,
        message: err.message,
      });
    }
  },

  async create(req, res) {
    try {
      if (req.body.name == null && req.body.sizeId == null && req.body.price == null) {
        res.status(400).json({
          status: false,
          message: "Size, Name and Price are required",
        });
        return;
      }
      // create car
      const data = await carService.create({
        sizeId: req.body.sizeId,
        name: req.body.name,
        price: req.body.price,
        deleted: false,
        createdAt: new Date(),
        updatedAt: new Date(),
      });
      const Log = await logService.create({
        carId: data.id,
        userId: req.user.id,
        createdBy: req.user.id,
        updatedBy: null,
        deletedBy: null,
      });
      const user = await userService.find(Log.userId);
      const userInfo = {
        id: user.id,
        email: user.email,
      };
      res.status(201).json({
        status: true,
        message: "Car has been created successfully",
        data: data,
        userInfo,
      });
    } catch (err) {
      res.status(422).json({
        status: false,
        message: err.message,
      });
    }
  },

  async show(req, res) {
    try {
      const data = await carService.get(req.params.id);
      if (data !== null) {
        const user = await userService.find(data.Logs[0].userId);
        console.log(user);
        const userInfo = {
          id: user.id,
          email: user.email,
        };
        res.status(200).json({
          status: true,
          message: 'Successfully find data',
          data: data,
          userInfo
        });
      } else {
        res.status(404).json({
          status: false,
          message: 'Data not found',
        });
      }
    } catch (error) {
      res.status(422).json({
        status: false,
        message: error.message,
      });
    }
  },

  async update(req, res) {
    try {
      if (req.body.name == null && req.body.sizeId == null && req.body.price == null) {
        res.status(400).json({
          status: false,
          message: "Size, Name and Price are required",
        });
        return;
      }
      await carService.update(req.params.id, req.body);
      await logService.update(req.params.id, {
        updatedBy: req.user.id,
        updatedAt: new Date(),
      });
      console.log(req.params.id);
      console.log(req.user.id);
      // get user info
      const user = await userService.find(req.user.id);
      const userInfo = {
        id: user.id,
        email: user.email,
      };
      // get car info
      const carUpdated = await carService.get(req.params.id);
      const data = {
        car: carUpdated,
        userInfo,
      };
      // get Log info
      res.status(200).json({
        status: true,
        message: "Car has been updated successfully",
        data: data,
      });
    } catch (err) {
      res.status(422).json({
        status: false,
        message: err.message,
      });
    }
  },

  async destroy(req, res) {
    try {
      await carService.delete(req.params.id, {
        deleted: true,
      });
      await logService.update(req.params.id, {
        deletedBy: req.user.id,
        deletedAt: new Date(),
      });
      res.status(200).json({
        status: true,
        message: "Car has been deleted successfully",
      });
    } catch (err) {
      res.status(422).json({
        status: false,
        message: err.message,
      });
    }
  },
};
