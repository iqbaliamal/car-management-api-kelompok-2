const roleService = require('../../services/roleService');

class roleController {
  static getByID(req, res) {
    roleService.get(req.params.id)
      .then((type) => {
        res.status(200).json({
          code: 200,
          message: "OK",
          data: type
        });
      })
      .catch((error) => {
        console.error(error);
        res.status(500).json({
          code: 500,
          message: "Internal Server Error"
        });
      });
  }
  static async getAll(req, res) {
    try {
      const getAll = await roleService.getAll()
      res.status(200).json(getAll)
    } catch (error) {
      res.status(422).json({
        status: 'FAIL',
        message: error.message,
      })
    }
  }
  static async create(req, res) {
    try {
      const typeUser = await roleService.create(req.body)
      res.status(200).json(typeUser)
    } catch (error) {
      res.status(422).json({
        status: 'FAIL',
        message: error.message,
      })
    }
  }
}

module.exports = roleController;
