const userService = require('../../services/userService')
const bcrypt = require('bcrypt')

module.exports = {
  async createMember (req, res) {
    const existedUser = await userService.findByEmail(req.body.email)
    if (existedUser) {
      return res.status(400).send({
        status: false,
        message: 'Email telah digunakan',
      })
    }
    const hashedPassword = await bcrypt.hash(req.body.password, 10)
    const user = {
      roleId: 3,
      name: req.body.name,
      email: req.body.email,
      password: hashedPassword,
      createdAt: new Date(),
      updatedAt: new Date(),
    }
    try {
      await userService.create(user)
      res.status(201).json({
        status: true,
        message: 'member Created',
        data: user,
      })
    } catch (error) {
      res.status(400).send(error)
    }
  },

  async createAdmin (req, res){
    const existedUser = await userService.findByEmail(req.body.email)
    if (existedUser) {
      return res.status(400).send({
        status: false,
        message: 'Email telah digunakan',
      })
    }
    const hashedPassword = await bcrypt.hash(req.body.password, 10)
    const user = {
      roleId: 2,
      name: req.body.name,
      email: req.body.email,
      password: hashedPassword,
      createdAt: new Date(),
      updatedAt: new Date(),
    }
    try {
      await userService.create(user)
      res.status(201).json({
        status: true,
        message: 'admin Created',
        data: user,
      })
    } catch (error) {
      res.status(400).send(error)
    }
  }
}
