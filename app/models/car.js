'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Car extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Car.belongsTo(models.Size, {foreignKey: 'sizeId', as: 'size'});
      Car.hasMany(models.Log, { foreignKey: "carId" });
    }
  }
  Car.init({
    sizeId: DataTypes.INTEGER,
    name: DataTypes.STRING,
    price: DataTypes.STRING,
    photo: DataTypes.STRING,
    deleted: DataTypes.BOOLEAN
  }, {
    sequelize,
    modelName: 'Car',
  });
  return Car;
};