'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Log extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      // Log.hasMany(models.Car, {as: 'cars'});
      // Log.hasMany(models.User, {as: 'users'});
      // Log.hasMany(models.User, {foreignKey: 'userId', as: 'user'});
      // Log.hasMany(models.Car, {foreignKey: 'carId', as: 'car'});

    }
  }
  Log.init({
    carId: DataTypes.INTEGER,
    userId: DataTypes.INTEGER,
    createdBy: DataTypes.INTEGER,
    updatedBy: DataTypes.INTEGER,
    deletedBy: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Log',
  });
  return Log;
};