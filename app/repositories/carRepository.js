const { Car, Log } = require('../models');

module.exports = {
  findAll() {
    return Car.findAll({
      include: [{ model: Log }],
      where: {
        deleted:false,
      },
    });
  },
  getTotalCar() {
    return Car.count();
  },
  find(id) {
    return Car.findOne({
      include: [
        { model: Log }
      ],
      where: {
        id: id,
        deleted:false,
      },
    });
  },
  create(createArgs) {
    return Car.create(createArgs);
  },
  update(id, updateArgs) {
    return Car.update(updateArgs, {
      where: {
        id,
      },
    });
  },
  delete(id, status) {
    return Car.update(status, {
      where: {
        id,
      }
    });
    // return Car.destroy({ where: { id } });
  },
};

// class carRepository {
//   static findAllPartially(whereClause) {
//     return Car.findAndCountAll(whereClause);
//   }

//   static find(id) {
//     return Car.findByPk(id);
//   }

//   static create(dataObj) {
//     return Car.create(dataObj);
//   }

//   static update(id, dataObj) {
//     return Car.update(dataObj, { where: { id }});
//   }

//   static delete(id, deletor) {
//     return Car.update({
//       deleted: true,
//       deletedBy: deletor,
//       deletedAt: new Date()
//     },
//     { where: { id }});
//   }
// }

// module.exports = carRepository;
