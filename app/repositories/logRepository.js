const { Log } = require("../models");

module.exports = {
  create(createArgs) {
    return Log.create(createArgs);
  },
  update(carId, updateArgs) {
    return Log.update(updateArgs, {
      where: {
        carId,
      },
    });
  },
};
