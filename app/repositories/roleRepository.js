const { Role } = require('../models');

class roleRepository {
  static findAllPartially(whereClause) {
    return Role.findAndCountAll(whereClause);
  }

  static find(id) {
    return Role.findByPk(id);
  }

  static create(createArgs) {
    return Role.create(createArgs)
  }

  static update(id, dataObj) {
    return Role.update(dataObj, { where: { id }});
  }

  static delete(id, deletor) {
    return Role.update({
      deleted: true,
      deletedBy: deletor,
      deletedAt: new Date()
    },
    { where: { id }});
  }
}

module.exports = roleRepository;
