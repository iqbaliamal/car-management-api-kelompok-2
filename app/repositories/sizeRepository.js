const { Size } = require('../models');

class sizeRepository {
  static findAllPartially(whereClause) {
    return Size.findAndCountAll(whereClause);
  }

  static find(id) {
    return Size.findByPk(id);
  }

  static create(dataObj) {
    return Size.create(dataObj);
  }

  static update(id, dataObj) {
    return Size.update(dataObj, { where: { id }});
  }

  static delete(id, deletor) {
    return Size.update({
      deleted: true,
      deletedBy: deletor,
      deletedAt: new Date()
    },
    { where: { id }});
  }
}

module.exports = sizeRepository;