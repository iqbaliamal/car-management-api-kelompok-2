const { Role, User } = require('../models')

module.exports = {
  // create type user -> temporary
  createRole(createArgs) {
    return Role.create(createArgs)
  },

  update(id, updateArgs) {
    return User.update(updateArgs, {
      where: {
        id,
      },
    })
  },

  delete(id) {
    return User.destroy(id)
  },

  find(email) {
    return User.findOne(email)
  },

  findAll() {
    return User.findAll()
  },

  findId(id) {
    return User.findByPk(id)
  },

  getByEmail(email) {
    return User.findOne({
      where: {
        email,
      },
    })
  },

  create(user) {
    return User.create({
      roleId: user.roleId,
      name: user.name,
      email: user.email,
      password: user.password,
      createdAt: new Date(),
      updatedAt: new Date(),
    })
  },
}
