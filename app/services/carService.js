// const path = require("path");
// const fs = require("fs");
// const Resizer = require("../../middlewares/resizer");
const carRepository = require('../repositories/carRepository');
const { Log } = require("../models");

module.exports = {
  async list() {
    try {
      const cars = await carRepository.findAll({
        include: [{ model: Log }],
      });
      const totalCar = await carRepository.getTotalCar();

      return {
        cars,
        count: totalCar,
      };
    } catch (err) {
      throw err;
    }
  },

  get(id) {
    return carRepository.find(id);
  },

  create(requestBody) {
    return carRepository.create(requestBody);
  },

  update(id, requestBody) {
    return carRepository.update(id, requestBody);
  },

  delete(id, status) {
    return carRepository.delete(id, status);
  },
};


// class carService {
//   static async getList() {
//     try {
//       let whereClause = new Object({
//         where: {
//           deleted: false
//         },
//       });
//       // if (filter) {
//       //   whereClause.where.size_id = filter;
//       //   console.log(whereClause);
//       // }
//       let { count, rows } = await carRepository.findAllPartially(whereClause);
      
//       return {
//         data: rows,
//         total: count,
//       };
//     } catch (error) {
//       throw error;
//     }
//   }

//   static get(id) {
//     return carRepository.find(id);
//   }

//   static create(dataObj) {
//     try {
//       // Tulis kode logika anda disini
      
//     } catch (error) {
//       throw error;
//     }
//   }

//   static update(dataObj) {
//     try {
//       // Tulis kode logika anda disini
      
//     } catch (error) {
//       throw error;
//     }
//   }

//   static delete(dataObj) {
//     try {
//       // Tulis kode logika anda disini
      
//     } catch (error) {
//       throw error;
//     }
//   }
// }

// module.exports = carService;
