const logRepository = require("../repositories/logRepository");

module.exports = {
  create(createArgs) {
    return logRepository.create(createArgs);
  },
  update(id, requestBody) {
    return logRepository.update(id, requestBody);
  },
};
