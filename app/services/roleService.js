const roleRepository = require('../repositories/roleRepository');

class userRoleService {
  static getAll() {
    return roleRepository.findAllPartially();
  }
  static get(id) {
    return roleRepository.find(id);
  }
  static create(requestBody) {
    return roleRepository.create(requestBody)
  }
}

module.exports = userRoleService;
