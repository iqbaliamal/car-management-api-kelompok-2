const sizeRepository = require('../repositories/sizeRepository');

class sizeService {
  static getAll() {
    return sizeRepository.findAll();
  }
  static get(id) {
    return sizeRepository.find(id);
  }
}

module.exports = sizeService;
