const userRepository = require('../repositories/userRepository')

module.exports = {
  createRole(requestBody) {
    return userRepository.createRole(requestBody)
  },

  update(id, requestBody) {
    return userRepository.update(id, requestBody)
  },

  delete(id) {
    return userRepository.delete(id)
  },

  get(email) {
    return userRepository.find(email)
  },

  find(id) {
    return userRepository.findId(id)
  },

  findByEmail(email) {
    return userRepository.getByEmail(email)
  },
  create(user) {
    return userRepository.create(user)
  },
}
