'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
     await queryInterface.bulkInsert("roles", [
        {
          name: "SUPER ADMIN",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: "ADMIN",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: "MEMBER",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
     ]);
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('roles', null, {});
  }
};
