'use strict';
const bcrypt = require("bcrypt");

module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
     const hashedPassword = await bcrypt.hash("admin123", 10);
     await queryInterface.bulkInsert("users", [
       {
         roleId: 1,
         name: "SUPER ADMIN",
         email: "superadmin@gmail.com",
         password: hashedPassword,
         createdAt: new Date(),
         updatedAt: new Date(),
       },
     ]);
 
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
     await queryInterface.bulkDelete("users", null, {});
  }
};
