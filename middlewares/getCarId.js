const carService = require('../app/services/carService');

module.exports = {
  async getById(req, res, next) {
    try {
      const data = await carService.get(req.params.id);
      if (data !== null) {
        console.log(data);
        next();
      } else {
        res.status(404).json({
          status: false,
          message: 'Data not found',
        });
      }
    } catch (error) {
      res.status(400).json({
        status: false,
        message: err.message,
      });
    }
  }
}