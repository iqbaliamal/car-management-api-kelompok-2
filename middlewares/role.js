const jwt = require('jsonwebtoken')
const userService = require('../app/services/userService')


module.exports = {
  async authorize (req, res, next) {
    try {
      const bearerToken = req.headers.authorization
      const token = bearerToken.split('Bearer ')[1]
      const tokenPayload = jwt.verify(
        token,
        process.env.JWT_SIGNATURE_KEY || 'secret',
      )

      req.user = await userService.find(tokenPayload.id)

      if (!req.user) {
          res.status(401).json({
            message: 'Anda tidak punya akses (Unauthorized)',
          })
          return
      }

      next()
    } catch (error) {
      if (error.message.includes('jwt expired')) {
        res.status(401).json({ message: 'Token Expired' })
        return
      }

      res.status(401).json({
        message: 'Unauthorized',
      })
    }
  },

  async isSuperadmin(req, res, next) {
    try {
      // add token role
      const bearerToken = req.headers.authorization;
      console.log(bearerToken);
      const token = bearerToken.split('Bearer ')[1];
      const tokenPayload = jwt.verify(
        token,
        process.env.JWT_SIGNATURE_KEY || 'secret',
      );
      req.user = await userService.find(tokenPayload.id)
      if (!(req.user.roleId === 1)) {
        res.status(401).json({ message: 'Anda bukan superadmin' })
        return
      }
      next()
    } catch (error) {
      if (error.message.includes('jwt expired')) {
        res.status(401).json({ message: 'Token Expired' })
        return
      }
    }
  },

  async isSuperadminorAdmin(req, res, next) {
    try {
      const bearerToken = req.headers.authorization
      const token = bearerToken.split('Bearer ')[1]
      const tokenPayload = jwt.verify(
        token,
        process.env.JWT_SIGNATURE_KEY || 'secret',
      )

      req.user = await userService.find(tokenPayload.id)
      if (!(req.user.roleId === 1 || req.user.roleId === 2)) {
        res.status(401).json({ message: 'Anda bukan superadmin atau admin' })
        return
      }
      next()
    } catch (error) {
      if (error.message.includes('jwt expired')) {
        res.status(401).json({ message: 'Token Expired' })
        return
      }

      res.status(401).json({
        message: 'Unauthorized',
      })
    }
  },
}