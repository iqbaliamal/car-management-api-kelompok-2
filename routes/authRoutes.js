const router = require('express').Router();
const authController = require('../app/controllers/api/auth/authController');
const roleMiddleware = require('../middlewares/role');
const userController = require('../app/controllers/api/userController');

// login users
router.post('/login', authController.login);

// admin add
router.post(
  '/register',
  roleMiddleware.isSuperadmin,
  userController.createAdmin,
);
// check current user
router.get(
  '/whoami',
  roleMiddleware.authorize,
  authController.whoAmI,
);


module.exports = router;
