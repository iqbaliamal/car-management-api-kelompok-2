const router = require('express').Router();
const carController = require('../app/controllers/api/carController');
const carMiddleware = require('../middlewares/getCarId');
const roleMiddleware = require('../middlewares/role');

router.get(
  '/',
  roleMiddleware.authorize,
  carController.list,
)
router.get(
  '/:id',
  roleMiddleware.authorize,
  carController.show,
)
router.post(
  '/',
  roleMiddleware.isSuperadminorAdmin,
  carController.create,
)
router.put(
  '/:id',
  roleMiddleware.isSuperadminorAdmin,
  carMiddleware.getById,
  carController.update,
)
router.delete(
  '/:id',
  roleMiddleware.isSuperadminorAdmin,
  carMiddleware.getById,
  carController.destroy,
)
 
module.exports = router;
