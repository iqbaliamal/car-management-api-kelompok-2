const router = require('Express').Router();
const main = require('../app/controllers/api/main');
const YAML = require("yamljs");
const swaggerUi = require("swagger-ui-express");
const swaggerDocument = YAML.load("./openapi.yaml");

/* ========= Main Routes ========= */
router.use("/api/car", require("./carRoutes"));
router.use("/api/user", require("./userRoutes"));
router.use("/api/role", require("./roleRoutes"));
router.use("/api/carsize", require("./sizeRoutes"));
router.use("/api/auth", require("./authRoutes"));

/* ========= Open API Routes ========= */
// show open api format .json
router.get("/api/docs", (req, res)=>{
  res.status(200).json(swaggerDocument);
});
// show open api with Swagger UI
router.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocument));

router.use(main.onLost)
router.use(main.onError)

module.exports = router;
