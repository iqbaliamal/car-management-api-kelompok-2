const router = require('express').Router();
const roleController = require('../app/controllers/api/roleController');
const roleMiddleware = require('../middlewares/role');


router.get("/", roleController.getAll);
router.get("/:id", roleController.getByID);

router.post(
  '/add', 
  roleMiddleware.isSuperadmin,
  roleController.create
);

module.exports = router;

