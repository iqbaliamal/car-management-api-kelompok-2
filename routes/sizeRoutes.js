const router = require('express').Router();
const sizeController = require('../app/controllers/api/sizeController');

router.get("/", sizeController.getAll);
router.get("/:id", sizeController.getByID);

module.exports = router;