const router = require('express').Router();
const userController = require('../app/controllers/api/userController');

// Add Member
router.post(
  '/add',
  userController.createMember,
)

module.exports = router;
